# Home Task №2
---  

## Exercise №1 
* Return the count of negative numbers in next list [4, -9, 8, -11, 8] 
* Note: do not use conditionals or loops

The simplest way to count the negative elements is to turn the list into a string and count the minuses.

```python 
def count_negatives(numbers: list) -> int:
    return str(numbers).count('-')
```

Result:

``` 
There are 2 negative numbers in list [4, -9, 8, -11, 8]
```

## Exercise №2 
* You have first 5 best tennis players according APT rankings.
* Set the first-place player (at the front of the list) to last place and vice versa.

The best way to replace words is to use swap. It can also be done with slices or using pop and insert list methods (see examples below).

```python 
def change_positions(players: list):
    players[0], players[-1] = players[-1], players[0]
    print(players)
```

```python 
def change_positions_slice(players: list):
    players = players[-1:] + players[1:-1] + players[:1]
    print(players)
```
```python 
def change_positions_pop(players: list):
    first = players.pop(0)
    last = players.pop(-1)
    players.insert(0, last)
    players.append(first)

    print(players)
```
Result:

``` 
['Elina Svitolina', 'Simona Halep', 'Naomi Osaka', 'Karolina Pliskova', 'Ashleigh Barty']
```

## Exercise №3 
* Swap words "reasonable" and "unreasonable" in quote: 
  "The reasonable man adapts himself to the world; the unreasonable one persists in trying to adapt the world to himself."
* Note. Do not use \<string>.replace() function or similar

Two functions were created for task 3. One of the functions changes the first found words "reasonable" and "unreasonable", and the other searches and replaces in a loop. The second function is more universal, because we can replace all the necessary words in text.

```python
def swap(text: str) -> str:
    text_list = text.split()
    index1 = text_list.index('reasonable')
    index2 = text_list.index('unreasonable')
    text_list[index1], text_list[index2] = text_list[index2], text_list[index1]
    text = ' '.join(text_list)
    print(text)
    return text
```


```python 
def change(first, second, text: str) -> str:
    text_list = text.split()
    for i in range(len(text_list)):
        if text_list[i] == first:
            text_list[i] = second
        elif text_list[i] == second:
            text_list[i] = first
    text = ' '.join(text_list)
    print(text)
    return text
```

Result:

``` 
The unreasonable man adapts himself to the world; the reasonable one persists in trying to adapt the world to himself.
```