def change_positions(players: list):
    players[0], players[-1] = players[-1], players[0]

    print(players)


def change_positions_swap(players: list):
    temp = players[0]
    players[0] = players[-1]
    players[-1] = temp

    print(players)


def change_positions_pop(players: list):
    first = players.pop(0)
    last = players.pop(-1)
    players.insert(0, last)
    players.append(first)

    print(players)


def change_positions_slice(players: list):
    players = players[-1:] + players[1:-1] + players[:1]

    print(players)


if __name__ == "__main__":
    players = ['Ashleigh Barty', 'Simona Halep',
               'Naomi Osaka', 'Karolina Pliskova', 'Elina Svitolina']
    change_positions(players)
