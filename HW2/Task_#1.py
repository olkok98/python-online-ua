import numpy as np


def count_negatives(numbers: list) -> int:
    return str(numbers).count('-')


def count_negatives_sign(numbers: list) -> int:
    numbers = np.sign(numbers)
    return list(numbers).count(-1)


if __name__ == '__main__':
    input_numbers = [4, -9, 8, -11, 8]
    print(f'There are {count_negatives(input_numbers)}'
          f' negative numbers in list {input_numbers}')
