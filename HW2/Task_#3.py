def swap(text: str) -> str:
    text_list = text.split()
    index1 = text_list.index('reasonable')
    index2 = text_list.index('unreasonable')
    text_list[index1], text_list[index2] = text_list[index2], text_list[index1]
    text = ' '.join(text_list)
    print(text)
    return text


def change(first, second, text: str) -> str:
    text_list = text.split()
    for i in range(len(text_list)):
        if text_list[i] == first:
            text_list[i] = second
        elif text_list[i] == second:
            text_list[i] = first
    text = ' '.join(text_list)
    print(text)
    return text


if __name__ == '__main__':
    text = 'The reasonable man adapts himself to the world; the unreasonable '\
           'one persists in trying to adapt the world to himself.'
    text = change('reasonable', 'unreasonable', text)
