def call_once(f):
    def wrapper(a, b):
        # nonlocal result
        if not wrapper.has_run:
            wrapper.result = f(a, b)
            wrapper.has_run = True
            print(wrapper.result)
        else:
            print(wrapper.result)

    # result = 0
    wrapper.has_run = False
    return wrapper


@call_once
def sum_of_numbers(a, b):
    return a+b


if __name__ == "__main__":
    sum_of_numbers(15, 42)
    sum_of_numbers(999, 1000)
    sum_of_numbers(16, 133)
