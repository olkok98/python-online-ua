# Home Task №11
- [Home Task №11](#home-task-11)
  - [Exercise №1](#exercise-1)
  - [Exercise №2](#exercise-2)
  - [Exercise №3](#exercise-3)
  - [Exercise №4](#exercise-4)
 

## Exercise №1 

In the first task we can use two methods of calling inner function. We can call the nested function or return the object of nested function in enclosed function.

I implemented the return of the object in enclosed function.
  
[--->>> File with realization <<<---](Task_1.py)

```python
a = "I am global variable!"


def enclosing_funcion():
    a = "I am variable from enclosed function!"

    def inner_function():
        a = "I am local variable!"
        # nonlocal a
        # global a
        print(a)
    return inner_function


if __name__ == "__main__":
    function = enclosing_funcion()
    function()
``` 


## Exercise №2 

I wrote a decorator in which I store the variable last_result. Each time the function wrapper() is executed, the last_result is displayed and updated according to new data.
  
[--->>> Realization <<<---](Task_2.py)

Decorator remember_result:
```python 
def remember_result(f):
    def wrapper(*args):
        nonlocal last_result
        print(f"Last result ='{last_result}'")
        last_result = f(*args)

    last_result = None
    return wrapper
```

Main:
```python 
if __name__ == "__main__":
    sum_list('a', 'b')
    sum_list('abc', 'cde')
    sum_list(3, 4, 5)
```

Result:

``` 
Last result ='None'
Current result ='ab'
Last result ='ab'
Current result ='abccde'
Last result ='abccde'
Current result ='345'
```

## Exercise №3 

3 task is a bit like 2. Just need to store the value of whether the function was running.

  
[--->>> Realization <<<---](Task_3.py)

Decorator call_once:

```python 
def call_once(f):
    def wrapper(a, b):
        if not wrapper.has_run:
            wrapper.result = f(a, b)
            wrapper.has_run = True
            print(wrapper.result)
        else:
            print(wrapper.result)

    wrapper.has_run = False
    return wrapper
```

Main:

```python 
if __name__ == "__main__":
    sum_of_numbers(15, 42)
    sum_of_numbers(999, 1000)
    sum_of_numbers(16, 133)
```


Result:

``` 
57
57
57
```

## Exercise №4

The first time when we run mod_a.py, we get 5, because file mod_b.py has an override of x, and the file is imported last. If we change the value of x in the mod_b.py, we will display this number by executing the file mod_a.py. If we import using from file import   *, functions and variable packages are accessed directly. Therefore we will need to write not print(mod_c.x), but print(x).