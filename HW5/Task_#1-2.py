import enum


class SortOrder(enum.Enum):
    ascending = 'ascending'
    descending = 'descending'


def IsSorted(x: list, sort_order: enum.Enum) -> bool:
    if not isinstance(sort_order, SortOrder):
        print('Sort order must be: SortOrder.ascending'
              'or SortOrder.descending!!!\n')
        return False
    if len(x) == 0 or len(x) == 1:
        return True
    if sort_order == SortOrder.ascending:
        return True if all(x[i] <= x[i+1] for i in range(0, len(x)-1))\
            else False
    if sort_order == SortOrder.descending:
        return True if all(x[i] >= x[i+1] for i in range(0, len(x)-1))\
            else False


def Transform(x: list, sort_order: enum.Enum):
    if IsSorted(x, sort_order):
        for i in range(0, len(x)):
            x[i] += i


if __name__ == "__main__":
    x_asc = [1, 2, 3, 4, 5]
    x_des = [5, 4, 3, 2, 1]
    x_mix = [3, 2, 5, 4, 1]
    x = x_asc
    sort_order = SortOrder.ascending
    print('List {} sorted in {} order?\n{}'.format(
        x, sort_order.value, IsSorted(x, sort_order)))
    Transform(x, sort_order)
    print('Result of transforming: {}'.format(x))
