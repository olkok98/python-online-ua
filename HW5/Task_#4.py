def SumGeometricElements(x: float, t: float, limit: float) -> float:
    if t >= 1 or t < 0:
        return x
    sum = 0
    while (x > limit):
        sum += x
        x *= t
    return sum


if __name__ == '__main__':
    x = float(input('Please, enter x_start: '))
    t = float(input('Please, enter step between (0,1): '))
    lim = int(input('Please, enter limit: '))
    print('Result of calculatios is: {}'.
          format(SumGeometricElements(x, t, lim)))
