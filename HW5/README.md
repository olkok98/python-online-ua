# Home Task №5
- [Home Task №5](#home-task-5)
  - [Exercise №1](#exercise-1)
  - [Exercise №2](#exercise-2)
  - [Exercise №3](#exercise-3)
  - [Exercise №4](#exercise-4)

## Exercise №1 

* Create function **IsSorted**, determining whether a given array of integer values of arbitrary length is sorted in a given order (the order is set up by enum value **SortOrder**). Array and sort order are passed by parameters. Function does not change the array.

For the first task, I created a class in which I store enum values. The enum value is passed to the function as a parameter. If the passed enum is missing in the SortOreder class, an error message will be displayed.

```python 
class SortOrder(enum.Enum):
    ascending = 'ascending'
    descending = 'descending'


def IsSorted(x: list, sort_order: enum.Enum) -> bool:
    if not isinstance(sort_order, SortOrder):
        print('Sort order must be: SortOrder.ascending'
              'or SortOrder.descending!!!\n')
        return False
    if len(x) == 0 or len(x) == 1:
        return True
    if sort_order == SortOrder.ascending:
        return True if all(x[i] <= x[i+1] for i in range(0, len(x)-1))\
            else False
    if sort_order == SortOrder.descending:
        return True if all(x[i] >= x[i+1] for i in range(0, len(x)-1))\
            else False
```

Result:

``` 
List [1, 2, 3, 4, 5] sorted in ascending order?
True
```

## Exercise №2 

* Create function **Transform**, replacing the value of each element of an integer array with the sum of this element value and its index, only if the given array is sorted in the given order (the order is set up by enum value **SortOrder**). Array and sort order are passed by parameters. To check, if the array is sorted, the function **IsSorted** from the Task 1 is called.

In the second task, I added a function to the code from the first task. If the condition of the IsSorted function is not met, we do not make changes to the array.

```python 
def Transform(x: list, sort_order: enum.Enum):
    if IsSorted(x, sort_order):
        for i in range(0, len(x)):
            x[i] += i
```

Result:

``` 
List [1, 2, 3, 4, 5] sorted in ascending order?
True
Result of transforming: [1, 3, 5, 7, 9]
```

## Exercise №3 
* Create function **MultArithmeticElements**, which determines the multiplication of the first n elements of arithmetic progression of real numbers with a given initial element of progression a<sub>1</sub> and progression step t. a<sub>n</sub> is calculated by the formula (a<sub>n+1</sub> = a<sub>n</sub>+t).

The MultArithmeticElements function receives three parameters, performs calculations and returns the result.

```python 
def MultArithmeticElements(x: float, t: float, n: int) -> float:
    product = x
    for i in range(1, n):
        x += t
        product *= x
    return product
```

Result:

``` 
Please, enter x_start:5
Please, enter step:4
Please, enter n(quantity):3
Result of calculatios is: 585.0
```

## Exercise №4 

* Create function **SumGeometricElements**, determining the sum of the first elements of a decreasing geometric progression of real numbers with a given initial element of a progression a<sub>1</sub> and a given progression step t, while the last element must be greater than a given a<sub>lim</sub>. a<sub>n</sub> is calculated by the formula (a<sub>n+1</sub> = a<sub>n</sub> * t), 0 < t < 1.

The SumGeometricElements function uses the while loop, because there is a condition of a minimum value at which the calculation is stopped.


```python 
def SumGeometricElements(x: float, t: float, limit: float) -> float:
    if t >= 1 or t < 0:
        return x
    sum = 0
    while (x > limit):
        sum += x
        x *= t
    return sum
```

Result:

``` 
Please, enter x_start: 100
Please, enter step between (0,1): 0.5
Please, enter limit: 20
Result of calculatios is: 175.0
```