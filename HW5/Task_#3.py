def MultArithmeticElements(x: float, t: float, n: int) -> float:
    product = x
    for i in range(1, n):
        x += t
        product *= x
    return product


if __name__ == '__main__':
    x = float(input('Please, enter x_start:'))
    t = float(input('Please, enter step:'))
    n = int(input('Please, enter n(quantity):'))
    print('Result of calculatios is: {}'.
          format(MultArithmeticElements(x, t, n)))
