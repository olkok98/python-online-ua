# Home Task №1

- [X] Read the PEP-8.
- [x] Calculate the area of triangle.
---  

## Calculate Area of Triangle
To calculate the area of a triangle, we use Heron's formula. It gives the area of a triangle when the length of all three sides are known. 

Heron's formula states that the area of a triangle whose sides have lengths $`a,`$ $`b,`$ and $`c`$ is

```math 
A={\sqrt {s(s-a)(s-b)(s-c)}}
```

where $`s`$ is the semi-perimeter of the triangle; that is,

```math 
s={\frac {a+b+c}{2}}.
```


### Code
* Used Python math Module
* Added conditions to check whether the entered sides form a triangle
* Printed calculated value with precision 2

```python
import math


a = 4.5
b = 5.9
c = 9

if a + b > c and a + c > b and b + c > a:
    s = (a + b + c) / 2
    A = math.sqrt(s * (s - a) * (s - b) * (s - c))
    print("{0:.2f}".format(A))
else:
    print("This is not a triangle!!!")
    

```

### Result
```bash
Area of triangle = 11.58
```
