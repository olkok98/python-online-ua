import math


a = 4.5
b = 5.9
c = 9

if a + b > c and a + c > b and b + c > a:
    s = (a + b + c) / 2
    A = math.sqrt(s * (s - a) * (s - b) * (s - c))
    print("Area of triangle = {0:.2f}".format(A))
else:
    print("This is not a triangle!!!")
