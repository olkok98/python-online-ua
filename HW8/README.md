# Home Task №8
- [Home Task №8](#home-task-8)
  - [Task 1](#task-1)
  - [Task 2](#task-2)
  - [Task 3](#task-3)
  - [Task 4](#task-4)
  - [Task 5](#task-5)
  - [Task 6](#task-6)

## Task 1
![Task 1](images/t1.jpg)

_**If a is greater than b, nothing will happen, otherwise we will get the assert error 'Not enough'**_
## Task 2 
![Task 2](images/t2.jpg)
_**If everything is ok in the try block, we get two print outputs, otherwise only finally print**_

## Task 3 
![Task 3](images/t3.jpg)
_**There will be 2 because finally block  is executed first**_

## Task 4 
![Task 4](images/t4.jpg)
_**We will get an error because 'else' must be before the 'finally'**_

## Task 5 
![Task 5](images/t5.jpg)
_**We will get an execution error because the raise should not get a string, but one of the exceptions**_

## Task 6 
![Task 6](images/t6.jpg)
_**In this task we will receive the looped program, where the print from exception will be deduced if the incorrect file name is entered**_

