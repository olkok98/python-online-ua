# Home Task №6
- [Home Task №6](#home-task-6)
  - [Exercise №1](#exercise-1)
  - [Exercise №2](#exercise-2)

## Exercise №1 

* Implement a function, that receives changeable number of dictionaries (keys - letters, values - numbers) and combines them into one dictionary.
Dict values should be summarized in case of identical keys

For the first task, I used the *args parameter because it is used to pass any number of variables. Implementation with a nested loop works faster than using a counter.

```python 
def combine_dicts(*args) -> dict:
    result = {}
    for dict_i in args:
        for key, value in dict_i.items():
            result.setdefault(key, 0)
            result[key] += value
    return result
```

```python 
def combine_dicts_by_counter(*args) -> dict:
    result = Counter()
    for dict_i in args:
        result.update(dict_i)
    return dict(result)
```

Result:

``` 
dict_1 =  {'a': 100, 'b': 200}
dict_2 =  {'a': 200, 'c': 300}
dict_3 =  {'a': 300, 'd': 100}
dict_1+dict_2 = {'a': 300, 'b': 200, 'c': 300}
dict_1+dict_2+dict_3 = {'a': 600, 'b': 200, 'c': 300, 'd': 100}
```

## Exercise №2 

* Calculate the factorial for positive number n

For the second task, I created a function with loops and a recursive function.


```python 
class ListNode:
    def __init__(self, data=None, next=None):
        self.data = data
        self.next = next

    def __repr__(self):
        return repr(self.data)
```
I created the class SinglyLinkedList and the methods for it, which are shown below.

```python 
class SinglyLinkedList:
    def __init__(self):
        self.head = None
```
`__getitem__` - it is a magical method that makes it possible to obtain an element by index. Takes O(n) time.
```python 
def __getitem__(self, index):
    if index > len(self) - 1:
        return "Index out of range"
    current_val = self.head
    for i in range(index):
        current_val = current_val.next
    return current_val.data
```
`__getitem__` - it is a magical method that makes it possible to change an element by index. Takes O(n) time.
```python   
    def __setitem__(self, index, value):
        if index > len(self) - 1:
            return "Index out of range"
        current_val = self.head
        for i in range(index):
            current_val = current_val.next
        current_val.data = value
```
`__repr__` - it is a magical method that makes it possible to represent SinglyLinkedList. Takes O(n) time.
```python 
    def __repr__(self):
        nodes = []
        current = self.head
        while current:
            nodes.append(repr(current))
            current = current.next
        return '[' + '-> '.join(nodes) + ']'
```
`__len__` - it is a magical method that finds size of SinglyLinkedList. Takes O(n) time.
```python 
    def __len__(self):
        current = self.head
        count = 0
        while current:
            count += 1
            current = current.next
        return count
```
`__iter__` - it is a magical method that allows the SinglyLinkedList to be iterated. Takes O(n) time.
```python 
    def __iter__(self):
        current = self.head
        while current is not None:
            yield current.data
            current = current.next
```
`__reversed__` - The reversed() function allows us to process the items in a sequence in reverse order. It accepts a sequence and returns an iterator. Takes O(n) time.
```python 
    def __reversed__(self):
        yield from reversed(list(self))
```
`append()` - Insert a new element at the end of the list. Takes O(n) time.
```python 
    def append(self, data):
        if not self.head:
            self.head = ListNode(data=data)
            return
        current = self.head
        while current.next:
            current = current.next
        current.next = ListNode(data=data)
```
`prepend()` - Insert a new element at the beginning of the list. Takes O(1) time.
```python 
    def prepend(self, data):
        self.head = ListNode(data=data, next=self.head)
```
`find()` - Search for the first element with `data` matching `value`. Return True or False if not found. Takes O(n) time.
```python 
    def find(self, value):
        current = self.head
        while current and current.data != value:
            current = current.next
        if current is None:
            return False
        return True
```
`index()` - Search for the first element with `data` matching `value`. Return the element or `None` if not found. Takes O(n) time.
```python 
    def index(self, value):
        current = self.head
        i = 0
        while current and current.data != value:
            current = current.next
            i += 1
        if current is None:
            return None
        return i  # Will be None if not found
```
`remove_by_value()` - Remove the first occurrence of `value` in the list. Takes O(n) time.
```python 
    def remove_by_value(self, value):
        current = self.head
        prev = None
        while current and current.data != value:
            prev = current
            current = current.next
        if prev is None:
            self.head = current.next
            del current
        elif current:
            prev.next = current.next
            del current
```
`remove_by_index()` - Remove the first occurrence of `index` in the list. Takes O(n) time.
```python 
    def remove_by_index(self, index):
        if index > len(self) - 1:
            raise Exception('Error Index out of Range')
        current = self.head
        prev = None
        for i in range(index):
            prev = current
            current = current.next
        if prev is None:
            self.head = current.next
            del current
        elif current:
            prev.next = current.next
            del current
```
`clear()` - Reset to a new singly-linked list. Takes O(1) time.
```python 
    def clear(self):
        self.head = None
```

Result:

``` 

```