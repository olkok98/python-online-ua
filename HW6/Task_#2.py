class ListNode:
    def __init__(self, data=None, next=None):
        self.data = data
        self.next = next

    def __repr__(self):
        return repr(self.data)


class SinglyLinkedList:
    def __init__(self):
        self.head = None

    def __getitem__(self, index):
        if index > len(self) - 1:
            return "Index out of range"
        current_val = self.head
        for i in range(index):
            current_val = current_val.next
        return current_val.data

    def __setitem__(self, index, value):
        if index > len(self) - 1:
            return "Index out of range"
        current_val = self.head
        for i in range(index):
            current_val = current_val.next
        current_val.data = value

    def __repr__(self):
        nodes = []
        current = self.head
        while current:
            nodes.append(repr(current))
            current = current.next
        return '[' + '-> '.join(nodes) + ']'

    def __len__(self):
        current = self.head
        count = 0
        while current:
            count += 1
            current = current.next
        return count

    def __iter__(self):
        current = self.head
        while current is not None:
            yield current.data
            current = current.next

    def __reversed__(self):
        yield from reversed(list(self))

    def append(self, data):
        if not self.head:
            self.head = ListNode(data=data)
            return
        current = self.head
        while current.next:
            current = current.next
        current.next = ListNode(data=data)

    def prepend(self, data):
        self.head = ListNode(data=data, next=self.head)

    def insert_at_index(self, index, data):
        if index > len(self):
            print('Index out of range')
        if index == 0:
            self.head = ListNode(data=data, next=self.head)
        elif index == len(self):
            current = self.head
            while current.next:
                current = current.next
            current.next = ListNode(data=data)
        else:
            new_node = ListNode(data)
            current = self.head
            for i in range(index):
                prev = current
                current = current.next
            prev.next, new_node.next = new_node, current

    def find(self, value):
        current = self.head
        while current and current.data != value:
            current = current.next
        if current is None:
            return False
        return True

    def index(self, value):
        current = self.head
        i = 0
        while current and current.data != value:
            current = current.next
            i += 1
        if current is None:
            return None
        return i

    def remove_by_value(self, value):
        current = self.head
        prev = None
        while current and current.data != value:
            prev = current
            current = current.next
        if prev is None:
            self.head = current.next
            del current
        elif current:
            prev.next = current.next
            del current

    def remove_by_index(self, index):
        if index > len(self) - 1:
            raise Exception('Error Index out of Range')
        current = self.head
        prev = None
        for i in range(index):
            prev = current
            current = current.next
        if prev is None:
            self.head = current.next
            del current
        elif current:
            prev.next = current.next
            del current

    def reverse(self):
        prev = None
        current = self.head
        next_ = current.next
        while current:
            current.next = prev
            prev = current
            current = next_
            if next_:
                next_ = next_.next
        self.head = prev
        return self

    def clear(self):
        self.head = None


if __name__ == "__main__":
    lst = SinglyLinkedList()
    lst_r = SinglyLinkedList()
    lst.append(23)
    lst.append('a')
    lst.append(42)
    lst.append('X')
    lst.append('the')
    lst.append('end')
    print(lst)
    lst.insert_at_index(0, 'oleh')
    print(lst)
