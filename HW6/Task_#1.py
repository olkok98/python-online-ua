from collections import Counter


def combine_dicts(*args) -> dict:
    result = {}
    for dict_i in args:
        for key, value in dict_i.items():
            result.setdefault(key, 0)
            result[key] += value
    return result


def combine_dicts_by_counter(*args) -> dict:
    result = Counter()
    for dict_i in args:
        result.update(dict_i)
    return dict(result)


if __name__ == "__main__":
    dict_l = {"a": 100, "b": 200}
    dict_2 = {"a": 200, "c": 300}
    dict_3 = {"a": 300, "d": 100}
    print('dict_1 = ', dict_l)
    print('dict_2 = ', dict_2)
    print('dict_3 = ', dict_3)
    print('dict_1+dict_2 = {}'.format(combine_dicts(dict_l, dict_2)))
    print('dict_1+dict_2+dict_3 = {}'.
          format(combine_dicts(dict_l, dict_2, dict_3)))
