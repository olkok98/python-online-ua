# Home Task №10
- [Home Task №10](#home-task-10)
  - [Exercise №1](#exercise-1)
  - [Exercise №2](#exercise-2)
  - [Exercise №3](#exercise-3)

 

## Exercise №1 

In the first task I wrote a function that reads names line by line, then sorts and deletes empty lines. The implementation of the function is demonstrated below.
  
[--->>> Realization <<<---](Task_1.py)

```python
def sort_names(filepath):
    with open(filepath, 'r') as file:
        lines = file.readlines()
    lines.sort()
    lines = list(filter(('\n').__ne__, lines))
    with open("result\\sorted_names.txt", 'w') as file:
        file.writelines(lines)
``` 

Result:
![Task_1_Result](result/Task_1.jpg)

## Exercise №2 

Іn the second task, I created a function that finds the most common words in the text. If you specify the number of words, the most common words in the text will be displayed, otherwise the words with the largest number of occurrences in the text will be displayed.
  
[--->>> Realization <<<---](Task_2.py)

```python 
def top_words(filepath, top_num=None):
    with open(filepath, 'r') as file:
        words = {}
        for line in file:
            for word in line.split():
                key = word.translate(str.maketrans('', '',
                                     string.punctuation)).lower()
                words[key] = words.get(key, 0) + 1
    sorted_words = sorted(words.items(), key=lambda x: x[1], reverse=True)
    if top_num is None:
        max_value = max(words.values())
        result = []
        for key, value in words.items():
            if value == max_value:
                result.append(key)
        print('Maximum word occurrence: ', max_value)
        print('Most common words: ', result)
    else:
        print('{} Top words: {}'.format(top_num,
              [row[0] for row in sorted_words[0:top_num]]))
```

Result:

``` 
Maximum word occurrence:  11
Most common words:  ['donec']
5 Top words: ['donec', 'etiam', 'aliquam', 'aenean', 'maecenas']
```

## Exercise №3 

In the third task, I created the following functions:
* _**read_csv_to_list(filepath)**_ - function for reading data from CSV files in the list
* _**write_list_to_csv(filepath)**_ - function for writing data from list to csv file
* _**top_students(filepath, number_top)**_ - the function shows the best students. If the specified number of students (number_top), we get the required number of the best students. If the number of students is not specified, we will get the students with the highest grade.
* _**sort_by_age(filepath)**_ - function that sorts data by age of students and saves them to a file
Implementation of the task realized by using a list.

  
[--->>> Realization <<<---](Task_3.py)

Main:

```python 
if __name__ == "__main__":
    data = Path("data\\students.csv")
    top_num = 5
    print("Students with highest score: ", top_students(data))
    print("{} Top students: {}".format(top_num, top_students(data, top_num)))
    sort_by_age(data)
```

Result:

``` 
Students with highest score:  ['Josephina Medina']
5 Top students: ['Josephina Medina', 'Teresa Jones', 'Richard Snider', 'Jessica Dubose', 'Heather Garcia']
```