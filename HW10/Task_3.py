from pathlib import Path
import csv


def read_csv_to_list(filepath):
    with open(filepath, 'r') as file:
        reader = csv.reader(file)
        return list(reader)


def write_list_to_csv(input_list, filepath):
    with open(filepath, 'w', newline='', encoding='utf-8') as file:
        writer = csv.writer(file, delimiter=',')
        for row in input_list:
            writer.writerow(row)


def top_students(filepath, number_top=None):
    csv_list = read_csv_to_list(filepath)
    csv_list.pop(0)
    csv_list.sort(key=lambda x: float(x[2]), reverse=True)
    if number_top is None:
        best = float(csv_list[0][2])
        return [row[0] for row in csv_list if float(row[2]) == best]
    else:
        return [row[0] for row in csv_list[0:number_top]]
    # operator itemgetter
    # csv_list[1:].sort()


def sort_by_age(filepath):
    csv_list = read_csv_to_list(filepath)
    result = []
    result.append(csv_list.pop(0))
    result.extend(sorted(csv_list, key=lambda x: int(x[1]), reverse=True))
    write_list_to_csv(result, "result\\sorted_by_age.csv")
    # operator itemgetter
    # csv_list[1:].sort()


if __name__ == "__main__":
    data = Path("data\\students.csv")
    top_num = 5
    print("Students with highest score: ", top_students(data))
    print("{} Top students: {}".format(top_num, top_students(data, top_num)))
    sort_by_age(data)
