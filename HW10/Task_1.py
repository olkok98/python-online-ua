from pathlib import Path


def sort_names(filepath):
    with open(filepath, 'r') as file:
        lines = file.readlines()
    lines.sort()
    lines = list(filter(('\n').__ne__, lines))
    with open("result\\sorted_names.txt", 'w') as file:
        file.writelines(lines)


if __name__ == "__main__":
    data = Path("data\\unsorted_names.txt")
    sort_names(data)
