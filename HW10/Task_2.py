from pathlib import Path
import string


def top_words(filepath, top_num=None):
    with open(filepath, 'r') as file:
        words = {}
        for line in file:
            for word in line.split():
                key = word.translate(str.maketrans('', '',
                                     string.punctuation)).lower()
                words[key] = words.get(key, 0) + 1
    sorted_words = sorted(words.items(), key=lambda x: x[1], reverse=True)
    if top_num is None:
        max_value = max(words.values())
        result = []
        for key, value in words.items():
            if value == max_value:
                result.append(key)
        print('Maximum word occurrence: ', max_value)
        print('Most common words: ', result)
    else:
        print('{} Top words: {}'.format(top_num,
              [row[0] for row in sorted_words[0:top_num]]))
        # print('Top words: ', sorted_words[0:top_num])


if __name__ == "__main__":
    data = Path("data\\lorem_ipsum.txt")
    top_words(data)
    top_words(data, 5)
