# Home Work №12
- [Home Work №12](#home-work-12)
  - [Task №1](#task-1)
  - [Task №2](#task-2)


## Task №1 

For the convenience of demonstrating the work in the console, so as not to take a lot of screenshots, I decided to use the **asciinema** application.

In step **b**, I executed the following commands and observed the specifics of output files and folders. Decryptions of used commands below:

* **ls** command – display files and directories that are not hidden.
* **ls \~** command - display files and directories that are not hidden in home directory.
* **ls -l** coomand - display detailed information of non-hidden files and directories.
* **ls -l /** command - display detailed information of non-hidden files and directories in root directory.
* **ls -a** command - display all files and directories in the current directory (including hidden).
* **ls -al** command - display detailed information of all files and directories in the current directory(including hidden)
* **ls -lda \~** command - display path to home directory

[![asciicast](https://asciinema.org/a/r71TttBFZhDxhKSD9OCSYQDCu.svg)](https://asciinema.org/a/r71TttBFZhDxhKSD9OCSYQDCu)

---

Decryptions of used commands in **task1.c** below:

* **mkdir** command – make directories
* **cd** command- change directorie
* **touch** command - create a file without any content
* **mv** command - move one or more files or directories from one place to another
* **cp** commad - used to copy files or group of files or directory
* **rm** command - used to remove files, directories
* **rm -rf** command - used to remove directories with content
* **rmdir** command - used to remove directories

[![asciicast](https://asciinema.org/a/Um2WE4NcsXEl0W62Ze54ybDnM.svg)](https://asciinema.org/a/Um2WE4NcsXEl0W62Ze54ybDnM)

---

In task 1.d I ran commands for **/etc/fstab**, but this file had not enough information to understand the difference between commands. So I decided to use another file (**/etc/services**). 

[![asciicast](https://asciinema.org/a/TFGR0k36EFkdFtuPKQaJnmylV.svg)](https://asciinema.org/a/TFGR0k36EFkdFtuPKQaJnmylV)

[![asciicast](https://asciinema.org/a/63PyVYoTxYS8WSeEhJZQQtq0R.svg)](https://asciinema.org/a/63PyVYoTxYS8WSeEhJZQQtq0R)

---

Manual pages for **cat**, **less** and **more**.
* **cat** command - reads data from the file and gives their content as output concatenate files and print on the standard output
* **less** command - displays text content, can be flipped up or down
* **more** command - displays text content and can be flipped down

[![asciicast](https://asciinema.org/a/8e5YbWYgBlMbVSRCsf15ApXp7.svg)](https://asciinema.org/a/8e5YbWYgBlMbVSRCsf15ApXp7)

## Task №2 

* Hard linked file is assigned the same Inode value as the original, therefore its reference the same physical file location. Hard link looks like copy of file that synchronize with original file and other hard links. If original file is removed then the links will still show the content of the file.

* A soft link can link to a directory or file. A soft link is similar to the shortcut in Windows. If the original file is deleted or moved, the soft linked file will not work.

[![asciicast](https://asciinema.org/a/V9H6ey5P3dWYfGLWi6caKodbr.svg)](https://asciinema.org/a/V9H6ey5P3dWYfGLWi6caKodbr)

---

I created executable .sh file, put lines with commands via vim, and ran it. 
**.sh** file makes it very easy to run a couple of commands.

[![asciicast](https://asciinema.org/a/OFQ1t17gHqBOCWovj3o3sxald.svg)](https://asciinema.org/a/OFQ1t17gHqBOCWovj3o3sxald)
