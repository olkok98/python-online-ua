class Point_In_Triangle:

    def __init__(self, x1, y1, x2, y2, x3, y3):
        self.x1 = x1
        self.y1 = y1
        self.x2 = x2
        self.y2 = y2
        self.x3 = x3
        self.y3 = y3

    def area(self, x1, y1, x2, y2, x3, y3):
        return abs((x1 * (y2 - y3) + x2 * (y3 - y1)
                    + x3 * (y1 - y2)) / 2.0)

    def validate_point(self, x: float, y: float):
        abc = self.area(self.x1, self.y1, self.x2, self.y2, self.x3, self.y3)
        pbc = self.area(x, y, self.x2, self.y2, self.x3, self.y3)
        pac = self.area(self.x1, self.y1, x, y, self.x3, self.y3)
        pab = self.area(self.x1, self.y1, self.x2, self.y2, x, y)

        if (abc == pbc + pac + pab):
            return True
        else:
            return False


if __name__ == "__main__":
    p_in_t = Point_In_Triangle(0, 0, -1, 1, 1, 1)

    x = float(input("Enter x: "))
    y = float(input("Enter y: "))

    print('Is point in shadow area: ', p_in_t.validate_point(x, y))
