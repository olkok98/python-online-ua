# First way with all conditionals
def fizz_buzz(num: int) -> str:
    if num < 1 or num > 100:
        return str(num) + ' is out of range[1, 100]'
    if num % 15 == 0:
        return 'FizzBuzz'
    elif num % 3 == 0:
        return 'Fizz'
    elif num % 5 == 0:
        return 'Buzz'
    return num


# Second way with string add.
def fizz_buzz_add_s(num: int) -> str:
    if num < 1 or num > 100:
        return str(num) + ' is out of range[1, 100]'
    s = ''
    if num % 3 == 0:
        s += 'Fizz'
    if num % 5 == 0:
        s += "Buzz"
    if s == '':
        s = num
    return s


# Best way.
def fizz_buzz_best(num: int) -> str:
    if num < 1 or num > 100:
        return str(num) + ' is out of range[1, 100]'
    s = 'Fizz'*(not(num % 3)) + "Buzz" * (not(num % 5))
    return str(num)*(not(s)) + s


if __name__ == "__main__":
    num = int(input('Enter the number:'))
    print(fizz_buzz(num))
