CARDS = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A']
VALUE = [2, 3, 4, 5, 6, 7, 8, 9, 0, 0, 0, 0, 1]


def play(hand: list) -> str:
    total_value = 0
    for card in hand:
        if card.upper() == 'JOKER':
            return 'Don\'t cheat!!!'
        if card.upper() not in CARDS:
            return 'Choose right cards!!!'
        total_value += VALUE[CARDS.index(card.upper())]
    total_value %= 10
    return str(total_value)


if __name__ == '__main__':
    player_cards = [input('Play first card: '), input('Play second card: ')]
    score = play(player_cards)
    print('Your result: ', score)
