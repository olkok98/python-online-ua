class Point_In_Triangle:

    def __init__(self, x1, y1, x2, y2, x3, y3):
        self.x1 = x1
        self.y1 = y1
        self.x2 = x2
        self.y2 = y2
        self.x3 = x3
        self.y3 = y3

    def sign(self, x_first, y_first, x_second, y_second, x_point, y_point):
        return (x_second - x_first) * (y_point - y_first) -\
               (y_second - y_first) * (x_point - x_first)

    def validate_point(self, x: float, y: float):
        ab = self.sign(self.x1, self.y1, self.x2, self.y2, x, y)
        bc = self.sign(self.x2, self.y2, self.x3, self.y3, x, y)
        ca = self.sign(self.x3, self.y3, self.x1, self.y1, x, y)

        point_in = (ab < 0 and bc < 0 and ca < 0) or\
            (ab > 0 and bc > 0 and ca > 0)
        point_edge = (ab == 0 and bc == 0) or\
            (ab == 0 and ca == 0) or (bc == 0 and ca == 0)
        point_peak =\
            (ca == 0 and (ab < 0 and bc < 0) or (ab > 0 and bc > 0))\
            or\
            (bc == 0 and (ab < 0 and ca < 0) or (ab > 0 and ca > 0))\
            or\
            (ab == 0 and (bc < 0 and ca < 0) or (bc > 0 and ca > 0))

        if point_in or point_edge or point_peak:
            return True
        else:
            return False


if __name__ == "__main__":
    p_in_t = Point_In_Triangle(0, 0, -1, 1, 1, 1)

    x = float(input("Enter x: "))
    y = float(input("Enter y: "))

    print('Is point in shadow area: ', p_in_t.validate_point(x, y))
