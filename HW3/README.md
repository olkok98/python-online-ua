# Home Task №3
- [Home Task №3](#home-task-3)
  - [Exercise №1](#exercise-1)
  - [Exercise №2](#exercise-2)
  - [Exercise №3](#exercise-3)

## Exercise №1 

* Write a program that determines whether the Point A(x,y) is in the shaded area or not.

Performing the first task, I decided to create a class for a more logical construction of the project. Below are two ways to implement the task. The first method is to calculate the areas of the formed triangles, this method is not optimal because there is tolerance in calculating the areas. The second method is to search for a point relative to the line, this method is more accurate.

```python 
class Point_In_Triangle:

    def __init__(self, x1, y1, x2, y2, x3, y3):
        self.x1 = x1
        self.y1 = y1
        self.x2 = x2
        self.y2 = y2
        self.x3 = x3
        self.y3 = y3

    def area(self, x1, y1, x2, y2, x3, y3):
        return abs((x1 * (y2 - y3) + x2 * (y3 - y1)
                    + x3 * (y1 - y2)) / 2.0)

    def validate_point(self, x: float, y: float):
        abc = self.area(self.x1, self.y1, self.x2, self.y2, self.x3, self.y3)
        pbc = self.area(x, y, self.x2, self.y2, self.x3, self.y3)
        pac = self.area(self.x1, self.y1, x, y, self.x3, self.y3)
        pab = self.area(self.x1, self.y1, self.x2, self.y2, x, y)

        if (abc == pbc + pac + pab):
            return True
        else:
            return False

```

```python 
class Point_In_Triangle:

    def __init__(self, x1, y1, x2, y2, x3, y3):
        self.x1 = x1
        self.y1 = y1
        self.x2 = x2
        self.y2 = y2
        self.x3 = x3
        self.y3 = y3

    def sign(self, x_first, y_first, x_second, y_second, x_point, y_point):
        return (x_second - x_first) * (y_point - y_first) -\
               (y_second - y_first) * (x_point - x_first)

    def validate_point(self, x: float, y: float):
        ab = self.sign(self.x1, self.y1, self.x2, self.y2, x, y)
        bc = self.sign(self.x2, self.y2, self.x3, self.y3, x, y)
        ca = self.sign(self.x3, self.y3, self.x1, self.y1, x, y)

        point_in = (ab < 0 and bc < 0 and ca < 0) or\
            (ab > 0 and bc > 0 and ca > 0)
        point_edge = (ab == 0 and bc == 0) or\
            (ab == 0 and ca == 0) or (bc == 0 and ca == 0)
        point_peak =\
            (ca == 0 and (ab < 0 and bc < 0) or (ab > 0 and bc > 0))\
            or\
            (bc == 0 and (ab < 0 and ca < 0) or (ab > 0 and ca > 0))\
            or\
            (ab == 0 and (bc < 0 and ca < 0) or (bc > 0 and ca > 0))

        if point_in or point_edge or point_peak:
            return True
        else:
            return False

```

Result:

``` 
Enter x: 0
Enter y: 0.5
Is point in shadow area:  True
```

## Exercise №2 

* Write a program that prints the input number from 1 to 100. But for multiple of three print "Fizz" instead of the number and for the multiples of five print "Buzz". For numbers which are multiples of both three and five print "FizzBuzz".

To accomplish this task, I have written several implementations:
* Simple conditions under which we show a different result.
* Under certain conditions, we add information to the string. At the end of function we return this string.
* For the last implementation, I used variables conversion and multiplication operations for the string.


```python 
# First way with all conditionals
def fizz_buzz(num: int) -> str:
    if num < 1 or num > 100:
        return str(num) + ' is out of range[1, 100]'
    if num % 15 == 0:
        return 'FizzBuzz'
    elif num % 3 == 0:
        return 'Fizz'
    elif num % 5 == 0:
        return 'Buzz'
    return num
```

```python 
# Second way with string add.
def fizz_buzz_add_s(num: int) -> str:
    if num < 1 or num > 100:
        return str(num) + ' is out of range[1, 100]'
    s = ''
    if num % 3 == 0:
        s += 'Fizz'
    if num % 5 == 0:
        s += "Buzz"
    if s == '':
        s = num
    return s
```
```python 
# Best way.
def fizz_buzz_best(num: int) -> str:
    if num < 1 or num > 100:
        return str(num) + ' is out of range[1, 100]'
    s = 'Fizz'*(not(num % 3)) + "Buzz" * (not(num % 5))
    return str(num)*(not(s)) + s
```
Result:

``` 
Enter the number:45
FizzBuzz
```

## Exercise №3 
- Simulate the one round of play for baccarat game
- Rules:
  - cards have a point value:
    - the 2 through 9 cards in each suit are worth face value (in points);
    - the 10, jack, queen and king have no point value (i.e. are worth zero);
    - aces are worth 1 point
  - Sum the values of cards. If total is more than 9 reduce 10 from result. E.g. 5 + 9 = 14, 14 - 10 = 4, 4 is the result
  - Player is not allowed to play another cards like joker

I made a function that calculates the value of the player's cards. It takes the list as input, so the number of cards is not limited. Any case(upper,lower,mixed) is allowed.

```python 
CARDS = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A']
VALUE = [2, 3, 4, 5, 6, 7, 8, 9, 0, 0, 0, 0, 1]


def play(hand: list) -> str:
    total_value = 0
    for card in hand:
        if card.upper() == 'JOKER':
            return 'Don\'t cheat!!!'
        if card.upper() not in CARDS:
            return 'Choose right cards!!!'
        total_value += VALUE[CARDS.index(card.upper())]
    total_value %= 10
    return str(total_value)


if __name__ == '__main__':
    player_cards = [input('Play first card: '), input('Play second card: ')]
    score = play(player_cards)
    print('Your result: ', score)

```

Result:

``` 
Play first card: 7
Play second card: 9
Your result:  6
```