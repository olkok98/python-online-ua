def factorial(num: int) -> str:
    if num < 0:
        return 'Sorry, factorial does not exist for negative numbers'
    elif num == 0 or num == 1:
        return 'The factorial of '+str(num)+' is '+str(fact)
    else:
        fact = 1
        for i in range(1, num + 1):
            fact = fact*i
        return 'The factorial of '+str(num)+' is '+str(fact)


def recur_factorial(num: int):
    if num == 0 or num == 1:
        return 1
    elif num < 1:
        return ('Sorry, factorial does not exist for negative numbers')
    else:
        return num*recur_factorial(num-1)


if __name__ == '__main__':
    num = int(input('Please, enter a number: '))
    print(factorial(num))
    print('The factorial of '+str(num)+' is '+str(recur_factorial(num)))
