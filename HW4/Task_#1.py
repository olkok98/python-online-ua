def transpose(A):
    B = [[0 for x in range(len(A))] for y in range(len(A[0]))]
    for i in range(len(A[0])):
        for j in range(len(A)):
            B[i][j] = A[j][i]
    return B


def transpose_short(A):
    return [[A[j][i] for j in range(len(A))] for i in range(len(A[0]))]


if __name__ == '__main__':
    A = [[0, 1, 9],
         [9, 8, 5],
         [7, 3, 1],
         [3, 4, 0]]

    B = [[0 for x in range(len(A))] for y in range(len(A[0]))]
    C = [[0 for x in range(len(A))] for y in range(len(A[0]))]

    B = transpose(A)
    C = transpose_short(A)

    print('Original matrix is:')
    for row in A:
        print(row)
    print('\nTransposed matrix is:')
    for row in B:
        print(row)
    print('\nTransposed matrix is:')
    for row in C:
        print(row)
