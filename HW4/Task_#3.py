def fibonacci(n):
    if n <= 0:
        print("Please enter a positive integer")
    a, b = 0, 1
    seq = []
    for i in range(0, n):
        seq.append(a)
        a, b = b, a+b
    print('Fibonacci sequence is', seq)
    print('Sum of elements in Fibonacci sequence is', sum(seq))


if __name__ == '__main__':
    num = input('Please, enter the length of sequence:')
    fibonacci(int(num))
