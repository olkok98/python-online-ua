# Home Task №4
- [Home Task №4](#home-task-4)
  - [Exercise №1](#exercise-1)
  - [Exercise №2](#exercise-2)
  - [Exercise №3](#exercise-3)
  - [Exercise №4](#exercise-4)

## Exercise №1 

* Write the script that can transpose any matrix.

* Note. The transpose of a matrix is an operator which flips a matrix over its diagonal; that is, it switches the row and column indices of the matrix A by producing another matrix
* Note: you could describe a matrix in Python using nested lists, e.g. matrix - [[0, 1, 2], [3, 4, 5]]

For the first task I made two functions, the first is classic (using loops), the second with a very short implementation.

```python 
def transpose(A):
    B = [[0 for x in range(len(A))] for y in range(len(A[0]))]
    for i in range(len(A[0])):
        for j in range(len(A)):
            B[i][j] = A[j][i]
    return B
```
```python 
def transpose_short(A):
    return [[A[j][i] for j in range(len(A))] for i in range(len(A[0]))]
```

Result:

``` 
Original matrix is:
[0, 1, 9]
[9, 8, 5]
[7, 3, 1]
[3, 4, 0]

Transposed matrix is:
[0, 9, 7, 3]
[1, 8, 3, 4]
[9, 5, 1, 0]
```

## Exercise №2 

* Calculate the factorial for positive number n

For the second task, I created a function with loops and a recursive function.


```python 
def factorial(num: int) -> str:
    if num < 0:
        return 'Sorry, factorial does not exist for negative numbers'
    elif num == 0 or num == 1:
        return 'The factorial of '+str(num)+' is '+str(fact)
    else:
        fact = 1
        for i in range(1, num + 1):
            fact = fact*i
        return 'The factorial of '+str(num)+' is '+str(fact)
```
```python 
def recur_factorial(num: int):
    if num == 0 or num == 1:
        return 1
    elif num < 1:
        return ('Sorry, factorial does not exist for negative numbers')
    else:
        return num*recur_factorial(num-1)
```

Result:

``` 
Please, enter a number: 10
The factorial of 10 is 3628800
```

## Exercise №3 
* For a positive integer n calculate the result value, which is equal to the sum of the first n Fibonacci numbers.

I used loops to find Fibonacci numbers. The recursive method requires an additional cycle to display intermediate calculations, so I didn't give this method in my homework.

```python 
def fibonacci(n):
    if n <= 0:
        print("Please enter a positive integer")
    a, b = 0, 1
    seq = []
    for i in range(0, n):
        seq.append(a)
        a, b = b, a+b
    print('Fibonacci sequence is', seq)
    print('Sum of elements in Fibonacci sequence is', sum(seq))
```

Result:

``` 
Please, enter the length of sequence:7
Fibonacci sequence is [0, 1, 1, 2, 3, 5, 8]
Sum of elements in Fibonacci sequence is 20
```

## Exercise №4 
* For a positive integer n calculate the result value, which is equal to the sum of the '1' in the binary representation of n.

* Note. Do not use any format functions

To find binary values, I wrote two functions. The first uses the while loop. The second is recursive and very concise.


```python 
def dec_to_bin(num: int):
    result = ''
    if num == 0:
        return 0
    while num > 0:
        result = str(num % 2)+result
        num = num // 2
    return result
```
```python 
def dec_to_bin_recur(num: int):
    return (dec_to_bin_recur(num // 2) if num > 1 else '')+str(num % 2)
``` 
Result:

``` 
Please, enter the positive decimal number:14
Number is 14, binary representation is 1110, sum is 3
```