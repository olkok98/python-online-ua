def dec_to_bin_recur(num: int):
    return (dec_to_bin_recur(num // 2) if num > 1 else '')+str(num % 2)


def dec_to_bin(num: int):
    result = ''
    if num == 0:
        return 0
    while num > 0:
        result = str(num % 2)+result
        num = num // 2
    return result


def sum_bin(s: str):
    res = 0
    for i in s:
        res += int(i)
    return res


if __name__ == '__main__':
    num = int(input('Please, enter the positive decimal number:'))
    binary_num = dec_to_bin_recur(num)
    print('Number is {}, binary representation is {}, sum is {}'.
          format(num, binary_num, sum_bin(binary_num)))
    binary_num = dec_to_bin(num)
    print('Number is {}, binary representation is {}, sum is {}'.
          format(num, binary_num, sum_bin(binary_num)))
