import string


def letters_in_all(array):
    chars = set(array[0])
    for text in array:
        chars.intersection_update(text)
    return chars if len(chars) != 0 else None


def all_letters(array):
    chars = set(array[0])
    for text in array:
        chars.update(text)
    return sorted(chars) if len(chars) != 0 else None


def every_in_two(array):
    temp = []
    for i in range(len(array)):
        for j in range(i+1, len(array)):
            temp.append(set(array[i]).intersection(array[j]))
    chars = set()
    for item in temp:
        chars.update(item)
    return sorted(chars) if len(chars) != 0 else None


def not_in_alphabet(array):
    alphabet = set(string.ascii_lowercase)
    for item in array:
        alphabet.difference_update(item.lower())
    return sorted(alphabet) if len(alphabet) != 0 else None


if __name__ == "__main__":
    text = ['hello', 'world', 'python']
    print('Input list: ', text)
    print('Characters that appear in all strings: ', letters_in_all(text))
    print('Characters that appear in at least one string: ', all_letters(text))
    print('Characters that appear at least in two strings: ',
          every_in_two(text))
    print('Characters of alphabet, that were not used in any string: ',
          not_in_alphabet(text))
