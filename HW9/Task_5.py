import string


def count_letters(text: str):
    frequency = {}
    text = text.translate(str.maketrans('', '', string.punctuation + ' \t\n'))
    for letter in text:
        frequency[letter] = text.count(letter)
    return frequency


if __name__ == "__main__":
    text = 'stringsample'
    print('Input string: ', text)
    print('Result :', count_letters(text))
