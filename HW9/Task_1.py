import timeit


def replace_quotation(text: str):
    result = ''
    for i in text:
        if i == '\"':
            result += '\''
        elif i == '\'':
            result += '\"'
        else:
            result += i
    return result


def replace_quotation_list(text: str):
    result = list(text)
    for i in range(len(result)):
        if result[i] == '\"':
            result[i] = '\''
        elif result[i] == '\'':
            result[i] = '\"'
    return ''.join(result)


def replace_quotation_trans(txt: str):
    mytable = str.maketrans('\'\"', '\"\'')
    txt = txt.translate(mytable)
    return txt


if __name__ == "__main__":
    text = '\"Hello\" \'World\' '
    print('Input string: ', text)
    print(replace_quotation(text))
    print(replace_quotation_list(text))
    print(replace_quotation_trans(text))

# print(timeit.timeit(lambda: replace_quotation(text), number=1000000))
# print(timeit.timeit(lambda: replace_quotation_list(text), number=1000000))
# print(timeit.timeit(lambda: replace_quotation_trans(text), number=1000000))
