def text_split(txt: str):
    stop = ' ,.;!?()[]\n\t'
    let_dict = {}
    i = 0
    length = 0
    for letter in txt:
        if letter in stop:
            if length > 0:
                let_dict[i-length] = length
                length = 0
        elif i == len(txt) - 1:
            length += 1
            let_dict[i-length] = length
        else:
            length += 1
        i += 1
    return let_dict


def shortes_word(txt: str):
    txt_dict = text_split(txt)
    min_i = min(txt_dict, key=txt_dict.get)
    last_element = min_i + txt_dict.get(min_i)
    return txt[min_i:last_element]


if __name__ == "__main__":
    a = 'The, quick [brown fox] jumps (over the) lazy dog'
    print('Input string: ', a)
    print('Shortest word in text: ', shortes_word(a))
