# Home Task №9
- [Home Task №9](#home-task-9)
  - [Exercise №1](#exercise-1)
  - [Exercise №2](#exercise-2)
  - [Exercise №3](#exercise-3)
  - [Exercise №4](#exercise-4)
  - [Exercise №5](#exercise-5)
 

## Exercise №1 

In the first task I wrote 3 implementations of functions using concatenation, joining list items to string and String translate(). Сoncatenation and list conversion work almost the same. String translate() works twice as fast.
  
[--->>> Realization <<<---](Task_1.py)

Main:

```python
if __name__ == "__main__":
    text = '\"Hello\" \'World\' '
    print('Input string: ', text)
    print(replace_quotation(text))
    print(replace_quotation_list(text))
    print(replace_quotation_trans(text))
``` 

Result:

``` 
Input string:  "Hello" 'World' 
'Hello' "World" 
'Hello' "World" 
'Hello' "World" 
```

## Exercise №2 

I wrote two functions to determine if a string is a palindrome. The first function replaces punctuation with replace(). The second uses String translate(). As in the first task, the implementation with translate() works faster.
  
[--->>> Realization <<<---](Task_2.py)

Main:

```python 
if __name__ == "__main__":
    text = '''Mr. Owl ate my
    metal worm'''
    print('Input string: ', text)
    print('It\'s palindrome: ', palindrome_check_trans(text))
```

Result:

``` 
Input string:  Mr. Owl ate my
    metal worm
It's palindrome:  True
```

## Exercise №3 

In the third task, I wrote a function for finding words lengths, and a function for finding the shortest word. First function returns dict, where key equal to index of the word in text and value equal to length of the word.
  
[--->>> Realization <<<---](Task_3.py)

Main:

```python 
if __name__ == "__main__":
    a = 'The, quick [brown fox] jumps (over the) lazy dog'
    print('Input string: ',a)
    print('Shortest word in text: ', shortes_word(a))
```

Result:

``` 
Input string:  The, quick [brown fox] jumps (over the) lazy dog
Shortest word in text:  The
```

## Exercise №4

To perform this task, I used the feature of sets and the functions difference() and intersection().
  
[--->>> Realization <<<---](Task_4.py)

Main:

```python 
if __name__ == "__main__":
    text = ['hello', 'world', 'python']
    print('Input list: ', text)
    print('Characters that appear in all strings: ', letters_in_all(text))
    print('Characters that appear in at least one string: ', all_letters(text))
    print('Characters that appear at least in two strings: ',
          every_in_two(text))
    print('Characters of alphabet, that were not used in any string: ',
          not_in_alphabet(text))
```

Result:

``` 
Input list:  ['hello', 'world', 'python']
Characters that appear in all strings:  {'o'}
Characters that appear in at least one string:  ['d', 'e', 'h', 'l', 'n', 'o', 'p', 'r', 't', 'w', 'y']
Characters that appear at least in two strings:  ['h', 'l', 'o']
Characters of alphabet, that were not used in any string:  ['a', 'b', 'c', 'f', 'g', 'i', 'j', 'k', 'm', 'q', 's', 'u', 'v', 'x', 'z']
```

## Exercise №5 

In task 5 I used the features of the dictionary (unique keys). I kept the letters in the keys, and number(count()) in the value.
  
[--->>> Realization <<<---](Task_5.py)

Main:

```python 
if __name__ == "__main__":
    text = 'stringsample'
    print('Input string: ', text)
    print('Result :', count_letters(text))
```

Result:

``` 
Input string:  stringsample
Result : {'s': 2, 't': 1, 'r': 1, 'i': 1, 'n': 1, 'g': 1, 'a': 1, 'm': 1, 'p': 1, 'l': 1, 'e': 1}
```