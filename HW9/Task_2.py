import string


def palindrome_check(txt: str):
    text = txt.replace(' ', '').replace(',', '').replace('\'', '').\
        replace('?', '').replace('!', '').replace('.', '').lower()
    return True if text == text[::-1] else False


def palindrome_check_trans(txt: str):
    # mytable = txt.maketrans('', '', ' ,.!?\t\'')
    # txt = txt.translate(mytable).lower()
    txt = txt.translate(
            str.maketrans('', '', string.punctuation + ' \t\n')).\
            lower()
    return True if txt == txt[::-1] else False


if __name__ == "__main__":
    text = '''Mr. Owl ate my
    metal worm'''
    print('Input string: ', text)
    print('It\'s palindrome: ', palindrome_check_trans(text))
