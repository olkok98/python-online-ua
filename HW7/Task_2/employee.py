class Employee:
    def __init__(self, name: str, salary: float):
        self.__name = name
        self.__salary = float(salary)
        self.__bonus = 0

    @property
    def name(self):
        return self.__name

    @property
    def salary(self):
        return self.__salary

    @property
    def bonus(self):
        return self.__bonus

    def set_bonus(self, bonus: float):
        self.__bonus = float(bonus)

    def to_pay(self):
        return self.__salary + self.__bonus


class SalesPerson(Employee):
    def __init__(self, name: str, salary: float, percent: int):
        super().__init__(name, salary)
        self.__percent = percent

    def set_bonus(self, bonus: float):
        if self.__percent > 200:
            super().set_bonus(3 * bonus)
        elif self.__percent > 100:
            super().set_bonus(2 * bonus)
        else:
            super().set_bonus(bonus)


class Manager(Employee):
    def __init__(self, name: str, salary: float, quantity: int):
        super().__init__(name, salary)
        self.__quantity = quantity

    def set_bonus(self, bonus: float):
        if self.__quantity > 150:
            super().set_bonus(bonus + 1000)
        elif self.__quantity > 100:
            super().set_bonus(bonus + 500)
        else:
            super().set_bonus(bonus)
