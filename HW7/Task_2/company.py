from employee import *


class Company:
    def __init__(self, *args):
        self.__list_of_employee = []
        for item in args:
            if isinstance(item, Employee):
                self.__list_of_employee.append(item)
            elif isinstance(item, list):
                if all(isinstance(sub_items, Employee) for sub_items in item):
                    self.__list_of_employee.extend(item)
            else:
                print('Invalid data for Company class!')
        self.__last_bonus = 0

    def give_everybody_bonus(self, bonus: float):
        if self.__last_bonus == 0:
            self.__last_bonus = bonus
            for person in self.__list_of_employee:
                person.set_bonus(person.bonus + bonus)
        else:
            for person in self.__list_of_employee:
                person.set_bonus(person.bonus +
                                 bonus - self.__last_bonus)

    def total_to_pay(self):
        return sum(person.to_pay() for person in self.__list_of_employee)

    def name_max_salary(self):
        maximum = 0
        for item in self.__list_of_employee:
            if item.to_pay() > maximum:
                maximum = item.to_pay()
        return [self.__list_of_employee[i].name for i, j in
                enumerate(self.__list_of_employee) if j.to_pay() == maximum]
