from employee import *
from company import *

if __name__ == "__main__":
    employee_1 = Employee('Huston', 2100)
    employee_1.set_bonus(100)
    employee_2 = Employee('Smith', 1600)
    sales_1 = SalesPerson('Wolfgan', 2800, 225)
    sales_2 = SalesPerson('Hoffman', 2000, 120)
    sales_1.set_bonus(100)
    manager = Manager('Trump', 2500, 120)
    manager.set_bonus(200)
    team = Company(employee_1, employee_2, sales_1, sales_2, manager)
    team.give_everybody_bonus(100)
    print('Total cost for salary: {}'.format(team.total_to_pay()))
    print('Name of employees with biggest salary: {}'.format(team.name_max_salary()))
