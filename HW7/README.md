# Home Task №7
- [Home Task №7](#home-task-7)
  - [Exercise №1](#exercise-1)
  - [Exercise №2](#exercise-2)

## Exercise №1 

For the first task, I created two implementations. The first, according to the given conditions(looks like for c😅), and the second more typical to Python.
  
[First implementaion (ArrayRectangles.py)](Task_1/ArrayRectangles.py)
  
[Second implementaion (ListRectangles.py)](Task_1/ListRectangles.py)

Each implementation contains:
  * class Rectangle, in which declare: 
    * 2 closed float sideA and sideB
    * Constructor with two parameters a and b(value b = 5 if not specified)
    * Method get_sideA(), returning value of the side A
    * Method get_sideB(), returning value of the side B
    * Method area(), calculating and returning the area value
    * Method perimeter(), calculating and returning the perimeter value
    * Method is_square(), checking whether current rectangle is shape square or not. Returns true if the shape is square and false in another case.
    * Method replace_sides(), swapping rectangle sides
  * class ArrayRectangles, in which declare:
    * Private field rectangles_array(rectangles_list)
    * Constructor creating an empty array of rectangles with length n **(only for array realization)**
    * Constructor that receives an arbitrary amount of objects of type Rectangle or an array of objects of type Rectangle.
    * Method add_rectangle() that adds a rectangle of type Rectangle to the array on the nearest free place and returning true, or returning false, if there is no free space in the array **(for list free place is unlimited)**
    * Method first_max_area() and numbers_max_area, that returns first index and list of indexes of the rectangle with the maximum area value
    * Method first_min_perimeter() and numbers_min_perimeter, that returns first index and list of indexes of the rectangle with the minimum area value
    * Method numbers_square() , that returns the numbers of squares in the array of rectangles

Main:

```python
from Rectangle import Rectangle
from ListRectangles import ListRectangles


if __name__ == "__main__":
    arr = [Rectangle(2, 4.6), Rectangle(5, 2), Rectangle(
        6, 6), Rectangle(9, 4), Rectangle(5.4, 3.1)]
    Arr_Rect = ListRectangles(arr)
    print('List of rectangles: {}'.format(Arr_Rect))
    Arr_Rect.numbers_min_perimetr()
    Arr_Rect.numbers_max_area()
    Arr_Rect.numbers_square()
``` 

Result:

``` 
List of rectangles: [[2.0, 4.6], [5.0, 2.0], [6.0, 6.0], [9.0, 4.0], [5.4, 3.1]]
Indexes of min perimetr: [0]
Indexes of maximum area: [2, 3]
Indexes of squares: [2]
```

## Exercise №2 

In the second task, I created the Employee class, two descendant classes SalesPerson and Manager. In parent class, I identified @property. Іn the descendants I used super() to refer to the parent class methods.
  
[Classes Employee, SalesPerson and Manager](Task_2/emoloyee.py)
  
[Class Company](Task_2/company.py)

* Basic class Employee:
  * Three closed fields — name, salary and bonus
  * Public property Name for reading employee's last name
  * Public property Salary for reading and recording salary field
  * Constructor with parameters string name and money salary
  * Method set_bomus that sets bonuses to salary
  * Method to_pay that returns the value of summarized salary and bonus
* Class SalesPerson as class Employee inheritor:
  * Closed integer field percent (percent of sales targets plan performance)
  * Constructor with parameters: name, salary, percent
  * Method set_bonus: if the sales person completed the plan more than 100%, so his bonus is doubled, and if more than 200% - bonus is tripled
* Class Manager as Employee class inheritor:
  * Closed integer field quantity (number of clients, who were served by the manager during a month)
  * Constructor with parameters name, salary and quantity
  * Method set_bonus: if the manager served over 100 clients, his bonus is increased by 500, and if more than 150 clients by 1000
* Class Company:
  * Closed list_of_employee, last_bonus(defines last company bonus)
  * Constructor that receives employees in different way (as parameters or list)
  * Method give_everybody_bonus with money parameter bonus that sets the amount of basic bonus for each employee
  * Method total_to_pay that returns total amount of salary of all employees including awarded bonus
  * Method name_max_salary that returns list of employees last name, who received maximum salary including bonus

Main:

```python 
from employee import *
from company import *

if __name__ == "__main__":
    employee_1 = Employee('Huston', 2100)
    employee_1.set_bonus(100)
    employee_2 = Employee('Smith', 1600)
    sales_1 = SalesPerson('Wolfgan', 2800, 225)
    sales_2 = SalesPerson('Hoffman', 2000, 120)
    sales_1.set_bonus(100)
    manager = Manager('Trump', 2500, 120)
    manager.set_bonus(200)
    team = Company(employee_1, employee_2, sales_1, sales_2, manager)
    team.give_everybody_bonus(100)
    print('Total cost for salary: {}'.format(team.total_to_pay()))
    print('Name of employees with biggest salary: {}'.format(team.name_max_salary()))

```

Result:

``` 
Total cost for salary: 14000.0
Name of employees with biggest salary: ['Wolfgan']
```