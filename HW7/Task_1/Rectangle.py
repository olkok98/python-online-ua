class ValueTooSmallError(Exception):
    """Raised when the input value is too small"""
    pass


class Rectangle:
    def __init__(self, a, b=5.0):
        try:
            self.__sideA = float(a)
            self.__sideB = float(b)
            if a <= 0 or b <= 0:
                raise ValueTooSmallError
        except ValueTooSmallError:
            print("Value of side is too small !")
        except ValueError:
            print("Invaild type of side!")

    def get_sideA(self):
        return self.__sideA

    def get_sideB(self):
        return self.__sideB

    def area(self):
        return self.__sideA * self.__sideB

    def perimeter(self):
        return 2 * (self.__sideA + self.__sideB)

    def is_square(self):
        return self.__sideA == self.__sideB

    def replace_sides(self):
        self.__sideA, self.__sideB = self.__sideB, self.__sideA
