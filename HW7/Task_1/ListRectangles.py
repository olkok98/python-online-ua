from Rectangle import Rectangle


class ListRectangles:
    def __init__(self, *args):
        self.__rectangles_list = []
        for rectangles in args:
            if isinstance(rectangles, Rectangle):
                self.__rectangles_list.append(rectangles)
            if isinstance(rectangles, list):
                self.__rectangles_list.extend(rectangles)

    def __repr__(self):
        temp = []
        for rectangle in self.__rectangles_list:
            temp.append(list([rectangle.get_sideA(), rectangle.get_sideB()]))
        return str(temp)

    def add_rectangle(self, rectangle):
        if not isinstance(rectangle, Rectangle):
            return
        self.__rectangles_list.append(rectangle)

    def first_max_area(self):
        area = 0
        index = 0
        for rectangle in self.__rectangles_list:
            if rectangle.area() > area:
                area = rectangle.area()
                index = self.__rectangles_list.index(rectangle)
        print('First max area index is {}'.format(index))
        return index

    def numbers_max_area(self):
        area = 0
        temp = []
        for rectangle in self.__rectangles_list:
            if rectangle.area() > area:
                area = rectangle.area()
        for rectangle in self.__rectangles_list:
            if rectangle.area() == area:
                temp.append(self.__rectangles_list.index(rectangle))
        print('Indexes of maximum area: {}'.format(temp))
        return temp

    def first_min_perimetr(self):
        perimeter = float('inf')
        index = 0
        for rectangle in self.__rectangles_list:
            if rectangle.perimeter() < perimeter:
                perimeter = rectangle.perimeter()
                index = self.__rectangles_list.index(rectangle)
        print('First min perimetr index is {}'.format(index))
        return index

    def numbers_min_perimetr(self):
        perimeter = float('inf')
        temp = []
        for rectangle in self.__rectangles_list:
            if rectangle.perimeter() < perimeter:
                perimeter = rectangle.perimeter()
        for rectangle in self.__rectangles_list:
            if rectangle.perimeter() == perimeter:
                temp.append(self.__rectangles_list.index(rectangle))
        print('Indexes of min perimetr: {}'.format(temp))
        return temp

    def numbers_square(self):
        squares = []
        for rectangle in self.__rectangles_list:
            if rectangle.is_square():
                squares.append(self.__rectangles_list.index(rectangle))
        print('Indexes of squares: {}'.format(squares))
        return squares
