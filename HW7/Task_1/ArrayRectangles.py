from Rectangle import Rectangle


class ArrayRectangles:  # like c =)
    # __rectangle_array
    def __init__(self, *args):
        self.__rectangles_array = []
        self.__to_end = 0
        if len(args) == 1 and isinstance(args[0], int):
            self.__to_end = args[0]
        else:
            for rectangle in args:
                if isinstance(rectangle, Rectangle):
                    self.__rectangles_array.append(rectangle)
                if isinstance(rectangle, list):
                    self.__rectangles_array.extend(rectangle)
        print(self.__to_end)

    def add_rectangle(self, rectangle):
        if not isinstance(rectangle, Rectangle):
            return
        elif self.__to_end > 0:
            self.__rectangles_array.append(rectangle)
            self.__to_end -= 1
            return True
        return False

    def first_max_area(self):
        area = 0
        index = 0
        for rectangle in self.__rectangles_array:
            if rectangle.area() > area:
                area = rectangle.area()
                index = self.__rectangles_array.index(rectangle)
        print('First max area index is {}'.format(index))
        return index

    def numbers_max_area(self):
        area = 0
        temp = []
        for rectangle in self.__rectangles_array:
            if rectangle.area() > area:
                area = rectangle.area()
        for rectangle in self.__rectangles_array:
            if rectangle.area() == area:
                temp.append(self.__rectangles_array.index(rectangle))
        print('Indexes of maximum area: {}'.format(temp))
        return temp

    def first_min_perimetr(self):
        perimeter = float('inf')
        index = 0
        for rectangle in self.__rectangles_array:
            if rectangle.perimeter() < perimeter:
                perimeter = rectangle.perimeter()
                index = self.__rectangles_array.index(rectangle)
        print('First min perimetr index is {}'.format(index))
        return index

    def numbers_min_perimetr(self):
        perimeter = float('inf')
        temp = []
        for rectangle in self.__rectangles_array:
            if rectangle.perimeter() < perimeter:
                perimeter = rectangle.perimeter()
        for rectangle in self.__rectangles_array:
            if rectangle.perimeter() == perimeter:
                temp.append(self.__rectangles_array.index(rectangle))
        print('Indexes of min perimetr: {}'.format(temp))
        return temp

    def numbers_square(self):
        squares = []
        for rectangle in self.__rectangles_array:
            if rectangle.is_square():
                squares.append(self.__rectangles_array.index(rectangle))
        print('Indexes of squares: {}'.format(squares))
        return squares
